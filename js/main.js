
        // ----------------- get JSON ------------------- //


$( document ).ready(function() {

    $.extend({
        getValues: function (url) {
            var result = null;
            $.ajax({
                url: 'json/sections.json',
                type: 'get',
                dataType: 'json',
                global: false,
                async: false,
                success: function (data) {
                    result = data;
                }
            });
            return result;
        }
    });

    var sectionsJSON = $.getValues("url string");



    $.extend({
        getValues: function (url) {
            var result = null;
            $.ajax({
                url: 'json/rooms.json',
                type: 'get',
                dataType: 'json',
                global: false,
                async: false,
                success: function (data) {
                    result = data;
                }
            });
            return result;
        }
    });

    var roomsJSON = $.getValues("url string");



    $.extend({
        getValues: function (url) {
            var result = null;
            $.ajax({
                url: 'json/devices.json',
                type: 'get',
                dataType: 'json',
                global: false,
                async: false,
                success: function (data) {
                    result = data;
                }
            });
            return result;
        }
    });

    var devicesJSON = $.getValues("url string");


        // ----------------- 1st Section ------------------- //


    var IDinSections1 = _.get(sectionsJSON, '[0].id');
    var rooms1 = _.filter(roomsJSON, _.matches({'sectionID': IDinSections1}));


        // ----------------- 1st Room ---------------- //

    var IDinRooms1 = _.get(rooms1, '[0].id');
    var devices1 = _.filter(devicesJSON, _.matches({'roomID': IDinRooms1}));
    console.log(devices1);

        // ----------------- 2st Room ---------------- //

    var IDinRooms1_2 = _.get(rooms1, '[1].id');
    var devices1_2 = _.filter(devicesJSON, _.matches({'roomID': IDinRooms1_2}));
    console.log(devices1_2);


        // ----------------- 2nd Section ------------------- //


    var IDinSections2 = _.get(sectionsJSON, '[1].id');
    var rooms2 = _.filter(roomsJSON, _.matches({'sectionID': IDinSections2}));

    var IDinRooms2 = _.get(rooms2, '[0].id');
    var devices2 = _.filter(devicesJSON, _.matches({'roomID': IDinRooms2}));



        // ----------------- 3rd Section ------------------- //


    var IDinSections3 = _.get(sectionsJSON, '[2].id');
    var rooms3 = _.filter(roomsJSON, _.matches({'sectionID': IDinSections3}));


    var IDinRooms3 = _.get(rooms3, '[0].id');
    var devices3 = _.filter(devicesJSON, _.matches({'roomID': IDinRooms3}));


        // ---- jquery > data binding ---- //


    // sections-names

    $(".section-1").html("<b>" + _.get(sectionsJSON, '[0].name') + "</b>");
    $(".section-2").html("<b>" + _.get(sectionsJSON, '[1].name') + "</b>");
    $(".section-3").html("<b>" + _.get(sectionsJSON, '[2].name') + "</b>");

    // section#1-rooms-names

    $(".section-1-room-1").html("<b>" + _.get(rooms1, '[0].name') + "</b>");
    $(".section-1-room-2").html("<b>" + _.get(rooms1, '[1].name') + "</b>");

    // section#1-room#1-devices-names

    $(".section-1-room-1-device-1").html(_.get(devices1, '[0].name'));
    $(".section-1-room-1-device-2").html(_.get(devices1, '[1].name'));
    $(".section-1-room-1-device-3").html(_.get(devices1, '[2].name'));

    // section#1-room#2-devices-names

    $(".section-1-room-2-device-1").html(_.get(devices1_2, '[0].name'));
    $(".section-1-room-2-device-2").html(_.get(devices1_2, '[1].name'));
    $(".section-1-room-2-device-3").html(_.get(devices1_2, '[2].name'));

    // section#1-room#1-devices

    $(".section1-panel-body-device-1").html("<div class='device-binary'>" +
        "<img src='img/kawa0.png' height='100px' id='kawa0'>" + "<img src='img/kawa100.png' height='100px' id='kawa100' class='coffee-on' >" +
        "<input type='checkbox' class='multi-switch-coffee' value='0' />" +
        "</div>");

    $(".section1-panel-body-device-2").html("<img class='light-bulb' src='img/light0.png' height='90px' />" +
        "<label class='switch'><input type='checkbox' class='multi-switch-bulb-1'><div class='slider round'></div></label>" +
        "<div id='slider-bulb-1'></div>");


    $(".section1-panel-body-device-3").html("<div class='device-binary'>" +
        "<img src='img/User10010.png' height='100px' id='switch1-0'>" + "<img src='img/User1001100.png' height='100px' id='switch1-100' class='switch1-on' >" +
        "<input type='checkbox' class='multi-switch-christmastree' value='0' />" +
        "</div>");


    // section#1-room#2-devices

    $(".section1-panel-body-device-2-1").html("<img class='light-bulb2' src='img/light0.png' height='90px' />" +
        "<label class='switch'><input type='checkbox' class='multi-switch-bulb-2'><div class='slider round'></div></label>" +
        "<div id='slider-bulb-2'></div>");


    // section#2-rooms-name

    $(".section-2-room-1").html("<b>" + _.get(rooms2, '[0].name') + "</b>");

    // section#2-room#1-devices-names

    $(".section-2-room-1-device-1").html(_.get(devices2, '[0].name'));
    $(".section-2-room-1-device-2").html(_.get(devices2, '[1].name'));
    $(".section-2-room-1-device-3").html(_.get(devices2, '[2].name'));
    $(".section-2-room-1-device-4").html(_.get(devices2, '[3].name'));

    // section#2-room#1-devices

    $(".section2-panel-body-device-1").html("<img class='light-bulb3' src='img/light0.png' height='90px' />" +
        "<label class='switch'><input type='checkbox' class='multi-switch-bulb-3'><div class='slider round'></div></label>" +
        "<div id='slider-bulb-3'></div>");

    $(".section2-panel-body-device-2").html("<div class='device-binary'>" +
        "<img src='img/zraszacz0.png' height='100px' id='drencher0'>" + "<img src='img/zraszacz100.png' height='100px' id='drencher100' class='drencher-on' >" +
        "<input type='checkbox' class='multi-switch-drencher' value='0' />" +
        "</div>");

    $(".section2-panel-body-device-3").html("<div class='device-binary'>" +
        "<img src='img/wilgotnosc.png' height='100px' id='humidity'>" + "<span class='humidity-value'></span>" +
        "</div>");

    $(".humidity-value").html(_.get(devices2, '[2].properties.value')+"%");

    $(".section2-panel-body-device-4").html("<img class='roulette1' src='img/roleta_zew0.png' height='90px' />" +
        "<label class='switch'><input type='checkbox' class='multi-switch-roulette-1'><div class='slider round'></div></label>" +
        "<div id='slider-roulette-1'></div>");


    // section#2-rooms-name

    $(".section-3-room-1").html("<b>" + _.get(rooms3, '[0].name') + "</b>");

    // section#2-room#1-devices-names

    $(".section-3-room-1-device-1").html(_.get(devices3, '[0].name'));
    $(".section-3-room-1-device-2").html(_.get(devices3, '[1].name'));

    // section#3-room#1-devices

    $(".section3-panel-body-device-1").html("<img class='light-bulb4' src='img/light0.png' height='90px' />" +
        "<label class='switch'><input type='checkbox' class='multi-switch-bulb-4'><div class='slider round'></div></label>" +
        "<div id='slider-bulb-4'></div>");

    $(".section3-panel-body-device-2").html("<div class='device-binary'>" +
        "<img src='img/lampa_ogrodowa0.png' height='100px' id='garder-light0'>" + "<img src='img/lampa_ogrodowa100.png' height='100px' id='garder-light100' class='garder-light-on' >" +
        "<input type='checkbox' class='multi-switch-garder-light' value='0' />" +
        "</div>");



       //   -------   devices' control    -------   //


    $('.multi-switch-coffee').multiSwitch({
        functionOnChange: function ($element) {

            $("#kawa100").removeClass("coffee-on").toggle()
            $("#kawa0").addClass("coffee-off").toggle()
        }
    });

            setTimeout(function() {
                $("#kawa100").removeClass("coffee-on").toggle()
                $("#kawa0").addClass("coffee-off").toggle()
            }, 10);


    setTimeout(function() {
        $("#light100").removeClass("light-on").toggle();
        $("#light-bulb").addClass("light-off").toggle()
    }, 10);



    $('.multi-switch-christmastree').multiSwitch({
        functionOnChange: function ($element) {

            $("#switch1-100").removeClass("switch1-on").toggle();
            $("#switch1-0").addClass("switch1-off").toggle()
        }
    });

            setTimeout(function() {
                $("#switch1-100").removeClass("switch1-on").toggle();
                $("#switch1-0").addClass("switch1-off").toggle()
            }, 10);


    $('.multi-switch-light5').multiSwitch({
        functionOnChange: function ($element) {

            $("#light100").removeClass("light5-on").toggle();
            $("#light-bulb").addClass("light5-off").toggle()
        }
    });

            setTimeout(function() {
                $("#light100").removeClass("light5-on").toggle();
                $("#light-bulb").addClass("light5-off").toggle()
            }, 10);


    $('.multi-switch-drencher').multiSwitch({
        functionOnChange: function ($element) {

            $("#drencher100").removeClass("drencher-on").toggle();
            $("#drencher0").addClass("drencher-off").toggle()
        }
    });

            setTimeout(function() {
                $("#drencher100").removeClass("drencher-on").toggle();
                $("#drencher0").addClass("drencher-off").toggle()
            }, 10);

    $('.multi-switch-garder-light').multiSwitch({
        functionOnChange: function ($element) {

            $("#garder-light100").removeClass("garder-light-on").toggle();
            $("#garder-light0").addClass("garder-light-off").toggle()
        }
    });

            setTimeout(function() {
                $("#garder-light100").removeClass("garder-light-on").toggle();
                $("#garder-light0").addClass("garder-light-off").toggle()
            }, 10);

            //   -------  UI slider    -------   //


    $(function () {
        $('#slider-bulb-1').slider({
            disabled: true,
            value: 0,
            min: 0,
            max: 100,

            step: 1,
            range: 'min',
            animate: true,
            change: function (event, ui) {

                switch (true) {
                    case (ui.value < 1):
                        $('.light-bulb').attr('src', 'img/light0.png');
                        break;
                    case (ui.value > 1.1 && ui.value < 33):
                        $('.light-bulb').attr('src', 'img/light30.png');
                        break;
                    case (ui.value > 34 && ui.value < 66):
                        $('.light-bulb').attr('src', 'img/light70.png');
                        break;
                    case (ui.value > 67 && ui.value <= 100):
                        $('.light-bulb').attr('src', 'img/light100.png');
                        break;
                }

            }
        });

    });

    $(function () {
        $('#slider-bulb-2').slider({
            value: 0,
            min: 0,
            max: 100,
            step: 1,
            range: 'min',
            animate: true,
            change: function (event, ui) {

                switch (true) {
                    case (ui.value < 1):
                        $('.light-bulb2').attr('src', 'img/light0.png');
                        break;
                    case (ui.value > 1.1 && ui.value < 33):
                        $('.light-bulb2').attr('src', 'img/light30.png');
                        break;
                    case (ui.value > 34 && ui.value < 66):
                        $('.light-bulb2').attr('src', 'img/light70.png');
                        break;
                    case (ui.value > 67 && ui.value <= 100):
                        $('.light-bulb2').attr('src', 'img/light100.png');
                        break;
                }

            }
        });

    });

    $(function () {
        $('#slider-bulb-3').slider({
            value: 0,
            min: 0,
            max: 100,
            step: 1,
            range: 'min',
            animate: true,
            change: function (event, ui) {

                switch (true) {
                    case (ui.value < 1):
                        $('.light-bulb3').attr('src', 'img/light0.png');
                        break;
                    case (ui.value > 1.1 && ui.value < 33):
                        $('.light-bulb3').attr('src', 'img/light30.png');
                        break;
                    case (ui.value > 34 && ui.value < 66):
                        $('.light-bulb3').attr('src', 'img/light70.png');
                        break;
                    case (ui.value > 67 && ui.value <= 100):
                        $('.light-bulb3').attr('src', 'img/light100.png');
                        break;
                }

            }
        });

    });


    $(function () {
        $('#slider-bulb-4').slider({
            value: 0,
            min: 0,
            max: 100,
            step: 1,
            range: 'min',
            animate: true,
            change: function (event, ui) {

                switch (true) {
                    case (ui.value < 1):
                        $('.light-bulb4').attr('src', 'img/light0.png');
                        break;
                    case (ui.value > 1.1 && ui.value < 33):
                        $('.light-bulb4').attr('src', 'img/light30.png');
                        break;
                    case (ui.value > 34 && ui.value < 66):
                        $('.light-bulb4').attr('src', 'img/light70.png');
                        break;
                    case (ui.value > 67 && ui.value <= 100):
                        $('.light-bulb4').attr('src', 'img/light100.png');
                        break;
                }

            }
        });

    });

    $(function () {
        $('#slider-roulette-1').slider({
            value: 0,
            min: 0,
            max: 100,
            step: 1,
            range: 'min',
            animate: true,
            change: function (event, ui) {

                switch (true) {
                    case (ui.value < 1):
                        $('.roulette1').attr('src', 'img/roleta_zew0.png');
                        break;
                    case (ui.value > 1.1 && ui.value < 24):
                        $('.roulette1').attr('src', 'img/roleta_zew30.png');
                        break;
                    case (ui.value > 25 && ui.value < 50):
                        $('.roulette1').attr('src', 'img/roleta_zew50.png');
                        break;
                    case (ui.value > 51 && ui.value <= 75):
                        $('.roulette1').attr('src', 'img/roleta_zew80.png');
                        break;
                    case (ui.value > 76 && ui.value <= 100):
                        $('.roulette1').attr('src', 'img/roleta_zew100.png');
                        break;
                }

            }
        });

    });


    $('.on-button').on('click', function() {
        $( "#slider" ).slider( "option", "disabled", false );
        $( "#slider" ).slider( "option", "value", 100 );
    });
    $('.off-button').on('click', function() {
        $( "#slider" ).slider( "option", "disabled", true );
        $( "#slider" ).slider( "option", "value", 0 );
    });



    $('.multi-switch-bulb-1').on('click', function() {
        if ($(this).is(":checked")) {
            $("#slider-bulb-1").slider("option", {
                disabled: false,
                value: 100
            });
        } else {
            $("#slider-bulb-1").slider("option", {
                disabled: true,
                value: 0
            });
        }
    });

    $('.multi-switch-bulb-2').on('click', function() {
        if ($(this).is(":checked")) {
            $("#slider-bulb-2").slider("option", {
                disabled: false,
                value: 100
            });
        } else {
            $("#slider-bulb-2").slider("option", {
                disabled: true,
                value: 0
            });
        }
    });

    $('.multi-switch-bulb-3').on('click', function() {
        if ($(this).is(":checked")) {
            $("#slider-bulb-3").slider("option", {
                disabled: false,
                value: 100
            });
        } else {
            $("#slider-bulb-3").slider("option", {
                disabled: true,
                value: 0
            });
        }
    });

    $('.multi-switch-bulb-4').on('click', function() {
        if ($(this).is(":checked")) {
            $("#slider-bulb-4").slider("option", {
                disabled: false,
                value: 100
            });
        } else {
            $("#slider-bulb-4").slider("option", {
                disabled: true,
                value: 0
            });
        }
    });

    $('.multi-switch-roulette-1').on('click', function() {
        if ($(this).is(":checked")) {
            $("#slider-roulette-1").slider("option", {
                disabled: false,
                value: 100
            });
        } else {
            $("#slider-roulette-1").slider("option", {
                disabled: true,
                value: 0
            });
        }
    });

});

            //   -------  accordion active class    -------   //


$(document).ready(function() {
    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).siblings('.panel-heading').addClass('active');
    });

    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.panel-heading').removeClass('active');
    });
});

$(document).ready(function() {
    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).siblings('.panel-default').addClass('active');
    });

    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.panel-default').removeClass('active');
    });
});

$(document).ready(function() {
    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).siblings('.panel-body').addClass('active');
    });

    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.panel-body').removeClass('active');
    });
});

$(document).ready(function() {
    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).siblings('.panel-default-device').addClass('active');
    });

    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.panel-default-device').removeClass('active');
    });
});
